import configparser
import distutils
import os
import time
import uuid
import requests
import sys
from datetime import datetime
from requests.auth import HTTPBasicAuth
from distutils import util
from constants import *

#configurazione
config = configparser.ConfigParser()
config.read('config.ini')
config_check = config['check']
config_dir = config['dir']

log_path = config_dir['logging_path']
log_file_name = 'cb_xdcr_' + time.strftime("%Y%m%d%H%M%S") + '_' + str(uuid.uuid1()) + '.log'

config_costanti = config['constants']
couchbase_url_reference = config_costanti['couchbase_url_reference']
couchbase_url_replica_bucket = config_costanti['couchbase_url_replica_bucket']

class ResponseScript(object):
    def __str__(self):
        return str(self.__dict__) + '\n'

    def __init__(self, message, hpoo, region_ocp, url):
        self.message = message
        self.HPOO = hpoo
        self.REGION_OCP = region_ocp
        self.URL = url


def main(argv):
    try:
        actor_inputs = {}
        for e in argv:
            key, value = e.split(':')
            key_upper = key.upper()
            actor_inputs[key_upper] = value

        validate_required(actor_inputs)
        
        # curl -X  POST -u admin:Spindox1! http://couchbase-spx-0000-couchbase-spx.apps.azuwepoc106l.6dba7406dcaf4075b252.westeurope.aksapp.io/pools/default/remoteClusters
        # -d username=admin-target -d password=Spindox1! -d hostname=couchbase-spx-target-0001.couchbase-spx-target.couchbase-spx-target.svc:8091 -d name=couchbase-spx-target -d demandEncryption=0
        host_value_cluster_source = 'http://' + actor_inputs.get(HOST_CS_KEY)
        if bool(distutils.util.strtobool(actor_inputs.get(FLAG_EXT_CT_KEY))):
            host_value_cluster_target = actor_inputs.get(HOST_CT_KEY) + ':31542' # 'y', 'yes', 't', 'true', 'on', and '1'
        else:
            host_value_cluster_target = actor_inputs.get(HOST_CT_KEY) + ':8091'

        # Perché settarli di default se sono obbligatori? 
        username_cs = actor_inputs.get(USERNAME_CS_KEY) if actor_inputs.get(USERNAME_CS_KEY) else "Administrator"                                                                                                   
        password_cs = actor_inputs.get(PASSWORD_CS_KEY) if actor_inputs.get(PASSWORD_CS_KEY) else "CB" + actor_inputs.get(SERVIZIO_DB_GSS_KEY) + actor_inputs.get(ENV_KEY) + "Admin"

        username_ct = actor_inputs.get(USERNAME_CT_KEY) if actor_inputs.get(USERNAME_CT_KEY) else "Administrator"
        password_ct = actor_inputs.get(PASSWORD_CT_KEY) if actor_inputs.get(PASSWORD_CT_KEY) else "CB" + actor_inputs.get(SERVIZIO_DB_GSS_KEY) + actor_inputs.get(ENV_KEY) + "Admin"

        nome_bucket_cs = actor_inputs.get(NOME_BUCKET_CS_KEY) if actor_inputs.get(NOME_BUCKET_CS_KEY) else actor_inputs.get(SERVIZIO_DB_GSS_KEY)
        nome_bucket_ct = actor_inputs.get(NOME_BUCKET_CT_KEY) if actor_inputs.get(NOME_BUCKET_CT_KEY) else actor_inputs.get(SERVIZIO_DB_GSS_KEY)

        api_url = host_value_cluster_source + couchbase_url_reference
        params = dict(
            demandEncryption='0',
            hostname=host_value_cluster_target,
            username=username_ct,
            password=password_ct,
            name=actor_inputs.get(NOME_REFERENCE_CT_KEY)
        )

        logger("Chiamata verso il cluster [ " + host_value_cluster_source + " ] per la creazione della reference del cluster target [ " +
                     host_value_cluster_target + " ]", False)
        response = requests.post(api_url, params=params, auth=HTTPBasicAuth(username_cs, password_cs))
        
        
        if (response.ok and (200 <= response.status_code <= 299)) or str(response.content).__contains__("Duplicate cluster"):
            api_url = host_value_cluster_source + couchbase_url_replica_bucket
            params = dict(
                enableCompression='1',
                replicationType='continuous',
                toBucket=nome_bucket_ct,
                toCluster=actor_inputs.get(NOME_REFERENCE_CT_KEY),
                fromBucket=nome_bucket_cs
            )

            # curl -v -X POST -u admin:Spindox1! http://couchbase-spx-0000-couchbase-spx.apps.azuwepoc106l.6dba7406dcaf4075b252.westeurope.aksapp.io/controller/createReplication
            # -d fromBucket=travel-sample -d toCluster=couchbase-spx-target -d toBucket=travel-sample -d replicationType=continuous -d enableCompression=1
            message = "Chiamata per la creazione della replica XDCR del bucket [ " +\
                  nome_bucket_cs + " ] del cluster source verso il bucket [ " +\
                  nome_bucket_ct + " ] del cluster target"
            logger(message, False)

            response = requests.post(api_url, params=params, auth=HTTPBasicAuth(username_cs, password_cs))
            logger(response.content, False)

            if not (response.ok and (200 <= response.status_code <= 299)):
                return ResponseScript('La creazione della replica del bucket [ ' + nome_bucket_ct + ' , cluster_target: ' + host_value_cluster_target + ' ] ' \
                        'con il bucket [ ' + nome_bucket_cs + ' , cluster_source: ' + host_value_cluster_source + ' ] non è andata a buon fine.', 'KO', '', '')
        else:
            logger(response.content, False)
            return ResponseScript('La creazione della reference [ ' + actor_inputs.get(NOME_REFERENCE_CT_KEY) + ', cluster_target: ' + host_value_cluster_target + ' ] nel cluster [ ' + host_value_cluster_source + ' ] ' \
                        'non è andata a buon fine.', 'KO', '', '')

        return ResponseScript('Operazione completata', 'OK', '', '')
    except Exception as e:
        return ResponseScript(str(e), 'KO', '', '')

def validate_required(inputs):
    d_absent = []
    required = config_check['required_input'].split(',')
    for e in required:
        e_upper = e.upper()
        if not e_upper in inputs.keys():
            d_absent.append(e_upper)

    if len(d_absent) > 0:
        #logger('I parametri necessari per la corretta esecuzione dello script sono i seguenti:\n'
        #                '[host_cluster_source] [admin_cluster_source] [password_cluster_source] [nome_bucket_cluster_source] '
        #                '[podIP_cluster_target] [nome_cluster_target] [admin_cluster_target] [password_cluster_target] [nome_bucket_cluster_target]\n', True)
        message = "Mancano i seguenti parametri obbligatori: " + " - ".join(d_absent)
        logger(message, False)
        raise Exception(message)

    replace_value_env(inputs)

def replace_value_env(inputs):
    # controllo valorizzazione variabile ENV
    valid_domain_env = eval(config_check['acceptable_values_env'])

    env_value = inputs.get(ENV_KEY).lower()
    for key, value in valid_domain_env.items():
        if env_value == key.lower():
            inputs[ENV_KEY] = value.lower()
            inputs[ENV_KEY + '_UP'] = value.upper()
            return

    raise Exception('Il valore passato in input nel campo ENV non è valido. [' + env_value + ']')



def logger(message="", v=False):
    """
    message: str
    v: boolean, optional

    Saves the message passed as a parameter in the
    log file defined on log_file_name. If v == true
    the message is also printed in the terminal
    """
    if not os.path.isdir(log_path):
        os.makedirs(log_path)

    now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    # Sanity check
    if isinstance(message, str) == False:
        message = str(message)

    log_text = now + "\t" + message + "\n"

    # Save log file

   
    with open(os.path.join(log_path, log_file_name), 'a+', encoding="utf-8") as f:
        f.write(log_text)

    if v == True:
        print(message)


if __name__ == "__main__":
    try:
        response = main(sys.argv[1:])
        logger(str(response), True)
    except Exception as e:
        logger('Errore: ' + str(e), True)
