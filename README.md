**Parametri di input di esempio - cluster OP interno**

```
host_cs:couchbase-spx-0000-couchbase-spx.apps.azuwepoc106l.6dba7406dcaf4075b252.westeurope.aksapp.io username_cs:admin password_cs:Spindox1! nome_bucket_cs:travel-sample host_ct:couchbase-spx-target-0001.couchbase-spx-target.couchbase-spx-target.svc nome_reference_ct:couchbase-spx-target username_ct:admin-target password_ct:Spindox1! nome_bucket_ct:travel-ok flag_external_ct:0
```

**Parametri di input di esempio - cluster OP esterno**
```
host_cs:couchbase-spx-0000-couchbase-spx.apps.azuwepoc106l.6dba7406dcaf4075b252.westeurope.aksapp.io username_cs:admin password_cs:Spindox1! nome_bucket_cs:travel-sample host_ct:10.1.32.4 nome_reference_ct:couchbase-spx-target username_ct:admin-target password_ct:Spindox1! nome_bucket_ct:travel-ok flag_external_ct:1
```




[Cross Data Center Replication (XDCR) | Couchbase Docs](https://docs.couchbase.com/server/current/learn/clusters-and-availability/xdcr-overview.html)

